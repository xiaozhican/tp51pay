<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
function alipay_config(){
    $config = array (
        //应用ID,您的APPID。
        'app_id' => "2021000117608553",

        //商户私钥
        'merchant_private_key' => "MIIEowIBAAKCAQEA0u7P0jm6Vner5VrIW9kD2yo7E4KWhyxT14nUDxV2taqPvCrhS7S0J2zxvWlcFzDqqr+BJCTbda55176zaEMRfx+tGpdUt9ZVCFYT1UfWHWmA1wSbUZqX3JR3k0vnMEJGTYQA1wvZlo4sJRXsNH9FibotsNdyIG+pSK2USFd0/7ssVIGXTBWqZHk3/uLqfEtfXx7PxIo/NElP6YSQqNp3uO7O2gJUe5A4uxDtag3+SOAW7rwHNM4ChXQPOwJayGVji+qwgqSoSbrRuiC7LI+92gsK36pgCcwaGW6uiNARn2lf1y2b4Ye6u0bvzRb288l2j3MBMIsCQc8holwHghOHAwIDAQABAoIBAFyNVLrVgaF3GQGl98v1SO/dc9Z3sjzVgmRMobOMo23KzQ6vcfOZ8oJqFxgZJsM2P4F0TeHiGjlkuSmlD2tsIj05VOcaB54yeVUTD9jBkH2PUW0HJG+F+aRbsKv83hAaxGY2yXeSR4L8Fvqex+mw/n4UJwA1SGpbFFhDcZDcbwXV+u4DYhbPv+aaudtbjvExz0FLRNMiVH5okDmqBubMDvq+70Oq8ospzLPQMqydxKlwMDZZ31f4Zj8Vb4sr4uMCl8Yuay90EksOCLm1o+M74q/x+K1cGJY6+JVaKu+OAk16UFuSHwDLuY8Tq/zUmt0O3QZRA3rtMEejYa5kfCvTiCECgYEA8Hvpiw1kSj0tbli6qNbrr/lAQDWw4Pgk4TwX6I2AoA5P18AJxnhYDtpmTzqdo8c4yR9V5s20mY6Y2JbHZqaTwFKC/+Fd4P/x1quGjPok2wutYVfVGI4Oz3esryU6dsEeqhqecp+Atzd8pRlm5fhtpbpXVNwy99NRbtUl5vK6ya0CgYEA4IrNGxMgIlBLmwReXPO+ngc7MvNiL1R9eCpcF87Vm1qWnVdXwY6X/b4UyN/YwCT0B1neFNlEXxrcXXyQ3LXoVPt1tUc5uswB/oNz/gtkGlvx+OAjkXXL5vTrDac8It4JfeIkKnpx5NRmcyZ9LrONuO17lyRfEB8wzgcF0TfbCW8CgYEAyj27M3jIrfJzIYi3OcS9Ixm/4gAFR70H7u+Fv8o4sOI4XV4gGmwcyij7v4exCQpDBEk5CchXKIiWnBd5jCdsvGHnz19tfwpgDMPlfOvlUN2qKuId0O+vHuUrMYRifXp92lpWhFkFe4/LYGFTevWI6MYkKbahmm953Xad0HKziWUCgYA4EdDnut3iU6Sdf/jEQ0VNFscskOPqp7FxCTrX4xkp4vQLNB1AwwenIAj2pBbu3AtnroQrNpnBGeb/HTTbOD1cwzIAY1nCEGFLGa4kONn1TxJcMDQRhZW2BWCQlzsIBePe4FJhnRPFrVgr1ueZetKf5AVhl355eyBnwg8s0ELM1wKBgEU4itAtdXg5mnHAEKHWZfhRgfGSDhXmIh51oC+ZYpxxOoFtY7q4R/32Li4seZ4zJUuCeDtYFfsnv3I6l06g0IfMUjdIsCGmRvVK1f5DPJi8Qpz4IeceSWy5dDxHNRi4nRp19D/YpJCNk2ll++qxpLkD2VxyCqGzwOAsDTIWEU0O",

        //异步通知地址
        'notify_url' => "http://45.40.251.208/index/notify/notify_url.html",

        //同步跳转
        'return_url' => "http://45.40.251.208/index/notify/return_url.html",

        //编码格式
        'charset' => "UTF-8",

        //签名方式
        'sign_type'=>"RSA2",

        //支付宝网关
        'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do",

        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0TxzsKSc1G+AjTF6xzYILAclMJaenJjXHPmVJ6AYqhf7EL3llIo9TzP9mNEmD4hwmrR4UHoozpMKfVGM4Ouq+oo2Rny7eapOkrWiZ7zEiRwBr+7TAjM90HgHVh/KWhlT5ROG4lP/2G786CLZbI4SWoPrYpPhWww6gVlrxGgw9jjyYLPC46FPQftydQKC/+/pFq6zsO0f21j3O66YjFS5+hvlzdRCcDtY/SUEhsZVq5phewaJx17v+bZ5IEhee3n43pWzTCZzwVad7ttcKYtM7ADMu1T8drnI0D8329+ktxndDwTzAwP8DYCkW0L4f0t0qOU9/Q8Yzb74524rUn/YPwIDAQAB",
    );
    return $config;
}