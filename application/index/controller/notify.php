<?php


namespace app\index\controller;


use think\Db;

class notify
{
    //异步回调
    public function notify_url()
    {
        require_once '../extend/AlipayTradePagePay/pagepay/service/AlipayTradeService.php';
        $config = alipay_config();
        $arr=$_POST;
        $alipaySevice = new \AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($_POST,true));
        $result = $alipaySevice->check($arr);
        if($result){
            //验证成功
            //获取支付宝的通知返回参数
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];
            $gmt_create = $_POST['gmt_create'];
            $gmt_payment = $_POST['gmt_payment'];
            $app_id = $_POST['app_id'];
            $receipt_amount = $_POST['receipt_amount'];
            $buyer_id = $_POST['buyer_id'];
            $seller_id = $_POST['seller_id'];
            $order = Db::name('Order')->where('out_trade_no',$out_trade_no)->find();
            if(!$order){
                exit('订单不存在！');
            }
            if($order['status'] == 2){
                exit('该订单已经支付成功了！');
            }
            if($trade_status == 'TRADE_SUCCESS'){
                //交易支付成功
                $res = Db::name('Order')->where('out_trade_no',$out_trade_no)->update([
                    'trade_no' => $trade_no,
                    'gmt_create' => $gmt_create,
                    'gmt_payment' => $gmt_payment,
                    'app_id' => $app_id,
                    'receipt_amount' => $receipt_amount,
                    'buyer_id' => $buyer_id,
                    'seller_id' => $seller_id,
                    'status' => 2
                ]);
                if($res){
                    echo "success";
                }else{
                    echo "fail";
                }
            }
        }else{
            //验证失败
            echo "fail";
        }
    }
    //
    public function return_url()
    {
        exit("<h1>支付完成</h1>");
    }
}