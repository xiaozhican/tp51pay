<?php


namespace app\index\controller;


use app\model\Order;
use think\Db;
use think\facade\Request;
use weixin\WxpayService;

class Pay
{
    public function index()
    {
        return view();
    }

    //支付宝电脑网站支付
    public function alipaypage()
    {
        require_once '../extend/AlipayTradePagePay/pagepay/service/AlipayTradeService.php';
        require_once '../extend/AlipayTradePagePay/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';
        $out_trade_no  = date('YmdHis').rand(100000,999999);
        $total_amount = $_POST['amount'];
        $subject = '购买商品';
        $body = "金币购买";
        $config = alipay_config();
        Db::name('Order')->insert([
            'out_trade_no' => $out_trade_no,
            'amount' => $total_amount,
            'subject' => $subject,
            'body ' => $body ,
            'create_time' => date('Y-m-d H:i:s')
        ]);
        //构造参数
        $payRequestBuilder = new \AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);

        $aop = new \AlipayTradeService($config);
        /**
         * pagePay 电脑网站支付请求
         * @param $builder 业务参数，使用buildmodel中的对象生成。
         * @param $return_url 同步跳转地址，公网可以访问
         * @param $notify_url 异步通知地址，公网可以访问
         * @return $response 支付宝返回的信息
         */
        $response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);

        //输出表单
        var_dump($response);
    }

}